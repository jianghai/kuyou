require.config({
  baseUrl: '/',
  paths: {
    'jquery': 'lib/jquery.min',
    'vjs': '../../vjs/vjs',
    'trunk': '../../kuyou/trunk/trunk',
    'jquery.extend': '../../jquery_extend/jquery.extend',
    'wordcloud': 'lib/wordcloud2',

    'chart': 'lib/chart.min',
    'highcharts': 'lib/highcharts',
    'd3': 'lib/d3.min',
    'daterange': '../../daterange/daterange',

    'base': '../../kuyou/trunk/components/base',
    'cookie': '../../kuyou/trunk/components/cookie',
    'Form': '../../kuyou/trunk/components/Form',
    'highcharts.line': '../../kuyou/trunk/components/highcharts.line',
    'highcharts.pie': '../../kuyou/trunk/components/highcharts.pie',
    'chart.line': '../../kuyou/trunk/components/chart.line',
    'select': '../../kuyou/trunk/components/select',
    'multiSelect': '../../kuyou/trunk/components/multiSelect',
    'dropdown': '../../kuyou/trunk/components/dropdown',
    'tab': '../../kuyou/trunk/components/tab',
    'grid': '../../kuyou/trunk/components/grid',
    'pagination': '../../kuyou/trunk/components/pagination',
    'search': '../../kuyou/trunk/components/search',
    'pin': '../../kuyou/trunk/components/pin',
    'datepicker': '../../kuyou/trunk/components/datepicker',
    'datepicker2': '../../kuyou/trunk/components/datepicker2',
    'dialog': '../../kuyou/trunk/components/dialog',
    'success': '../../kuyou/trunk/components/success',
    'error': '../../kuyou/trunk/components/error',
    'confirm': '../../kuyou/trunk/components/confirm',
    'popover': '../../kuyou/trunk/components/popover',
    'autocomplete': '../../kuyou/trunk/components/autocomplete',
  },
  shim: {
    'jquery.extend': ['jquery'],
    daterange: ['jquery'],
    d3: {
      exports: 'd3'
    },
    highcharts: {
      deps: ['jquery'],
      exports: 'Highcharts'
    }
  }
});